package com.ocr.medhead.apigateway;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.PathVariable;

@EnableDiscoveryClient
@SpringBootApplication
//@RestController
public class ApiGatewayApplication {
	
	@Autowired
	private DiscoveryClient discoveryClient;

	public static void main(String[] args) {
		SpringApplication.run(ApiGatewayApplication.class, args);
	}
	
	@Bean
	public RouteLocator medheadRoutes(RouteLocatorBuilder builder) {
		String msHospitalURI = serviceInstancesByApplicationName("ms-hospital").get(0).getUri().toString();
	    return builder.routes()
	    		.route(p -> p
	    				.path("/hospitals").or().path("/hospitals/*")
	    				.uri(msHospitalURI))
	    		.build();
	}
	
	//@RequestMapping("/service-instances/{applicationName}")
	public List<ServiceInstance> serviceInstancesByApplicationName(@PathVariable String applicationName) {
		return this.discoveryClient.getInstances(applicationName);
	}

}
