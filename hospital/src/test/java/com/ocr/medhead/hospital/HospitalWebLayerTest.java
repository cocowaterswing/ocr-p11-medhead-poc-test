/**
 * 
 */
package com.ocr.medhead.hospital;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

/**
 * @author bertrandnicolas
 *
 */

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class HospitalWebLayerTest {
	
	@LocalServerPort
	private int port;
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	@Test
	public void contextLoads() throws Exception {
		/*Hospital hospital = this.restTemplate.getForObject("http://localhost:" + port + "/hospitals/1", Hospital.class);
		assertEquals(hospital.getName(), "Fred Brooks");
		assertEquals(hospital.getBedsAvailable(), 2);
		
		Link link = linkTo(methodOn(HospitalController.class).one(hospital.getId())).withSelfRel();
		UriComponents components = UriComponentsBuilder.fromUriString(link.getHref()).build();
		assertEquals(components.getPath(), "/hospitals/1");*/

	}
	
}
