package com.ocr.medhead.hospital.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Hospital {
	
	private @Id @GeneratedValue Long id;
	private String name;
	private int bedsAvailable;
	private String[] specialties;
	
	Hospital(){}
	
	public Hospital(String name, int bedsAvailable, String[] specialties) {
		this.setName(name);
		this.setBedsAvailable(bedsAvailable);
		this.setSpecialties(specialties);
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;		
	}
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the bedsAvailable
	 */
	public int getBedsAvailable() {
		return bedsAvailable;
	}

	/**
	 * @param bedsAvailable the bedsAvailable to set
	 */
	public void setBedsAvailable(int bedsAvailable) {
		this.bedsAvailable = bedsAvailable;
	}

	/**
	 * @return the specialties
	 */
	public String[] getSpecialties() {
		return specialties;
	}

	/**
	 * @param specialties the specialties to set
	 */
	public void setSpecialties(String[] specialties) {
		this.specialties = specialties;
	}

}
