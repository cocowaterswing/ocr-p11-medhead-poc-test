/**
 * 
 */
package com.ocr.medhead.hospital.modelAssembler;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import com.ocr.medhead.hospital.controller.HospitalController;
import com.ocr.medhead.hospital.entity.Hospital;

/**
 * @author bertrandnicolas
 *
 */

@Component
public class HospitalModelAssembler implements RepresentationModelAssembler<Hospital, EntityModel<Hospital>>{

	@Override
	public EntityModel<Hospital> toModel(Hospital hospital) {
		
		return EntityModel.of(hospital,
				linkTo(methodOn(HospitalController.class).one(hospital.getId())).withSelfRel(),
				linkTo(methodOn(HospitalController.class).all()).withRel("hospitals"));
	}

}