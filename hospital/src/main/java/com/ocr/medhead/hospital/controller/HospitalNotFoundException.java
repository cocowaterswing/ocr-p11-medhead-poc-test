package com.ocr.medhead.hospital.controller;

class HospitalNotFoundException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	HospitalNotFoundException(Long id) {
		super("Could not find hospital " + id);
	}

}