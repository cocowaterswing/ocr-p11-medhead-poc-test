package com.ocr.medhead.hospital.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ocr.medhead.hospital.entity.Hospital;

public interface HospitalRepository extends JpaRepository<Hospital, Long> {

}
