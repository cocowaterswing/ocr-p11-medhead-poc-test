/**
 * 
 */
package com.ocr.medhead.hospital.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.hateoas.mediatype.problem.Problem;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ocr.medhead.hospital.entity.Hospital;
import com.ocr.medhead.hospital.modelAssembler.HospitalModelAssembler;
import com.ocr.medhead.hospital.repository.HospitalRepository;

/**
 * @author bertrandnicolas
 *
 */

@RestController
public class HospitalController {
	
	private final HospitalRepository repository;
	private final HospitalModelAssembler assembler;
	
	HospitalController(HospitalRepository repository, HospitalModelAssembler assembler) {
		this.repository = repository;
		this.assembler = assembler;
	}
	
	// Aggregate root
	// tag::get-aggregate-root[]
	@GetMapping("/hospitals")
	public CollectionModel<EntityModel<Hospital>> all() {
		
		List<EntityModel<Hospital>> hospitals = repository.findAll().stream()
				.map(assembler::toModel)
				.collect(Collectors.toList());
		
		return CollectionModel.of(hospitals, linkTo(methodOn(HospitalController.class).all()).withSelfRel());
	}
	// end::get-aggregate-root[]

	@PostMapping("/hospitals")
	ResponseEntity<?> newHospital(@RequestBody Hospital newHospital) {
		EntityModel<Hospital> entityModel = assembler.toModel(repository.save(newHospital));
				
		return ResponseEntity.created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri()).body(entityModel);
	}

	@GetMapping("/hospitals/{id}")
	public EntityModel<Hospital> one(@PathVariable Long id) {
		Hospital hospital = repository.findById(id).orElseThrow(() -> new HospitalNotFoundException(id));
		
		return assembler.toModel(hospital);
	}

	@PutMapping("/hospitals/{id}")
	ResponseEntity<?> replaceHospital(@RequestBody Hospital newHospital, @PathVariable Long id) { 
		Hospital updatedHospital =  repository.findById(id)
				.map(hospital -> {
			    	hospital.setName(newHospital.getName());
			    	hospital.setBedsAvailable(newHospital.getBedsAvailable());
			    	hospital.setSpecialties(newHospital.getSpecialties());
			    	return repository.save(hospital);})
				.orElseGet(() -> {
					newHospital.setId(id);
					return repository.save(newHospital);
				});
		
		EntityModel<Hospital> entityModel = assembler.toModel(updatedHospital);
		
		return ResponseEntity.created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri()).body(entityModel);
	}

	@DeleteMapping("/hospitals/{id}")
	ResponseEntity<?> deleteHospital(@PathVariable Long id) {
		ResponseEntity<Object> entity = ResponseEntity.noContent().build();
		
		try {
			repository.deleteById(id);
		} catch (Exception ex) {
			Logger log = LoggerFactory.getLogger(HospitalController.class);
			log.warn("" + new HospitalNotFoundException(id));
			entity = ResponseEntity.status(HttpStatus.BAD_REQUEST)
				      .body(Problem.create()
				          .withTitle("Server cannot process the request")
				          .withDetail("" + new HospitalNotFoundException(id)));
		}
		
		return entity;
	}

	int methodNotTested() {
		return 1;		
	}
	
}
