/**
 * 
 */
package com.ocr.medhead.hospital.loadData;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ocr.medhead.hospital.entity.Hospital;
import com.ocr.medhead.hospital.repository.HospitalRepository;

/**
 * @author bertrandnicolas
 *
 */

@Configuration
class LoadDatabase {
	
	private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

	@Bean
	CommandLineRunner initDatabase(HospitalRepository repository) {
		return args -> {
			repository.save(new Hospital("Fred Brooks", 2, new String[] {"Cardiologie", "Immunologie"}));
			repository.save(new Hospital("Julia Crusher", 0, new String[] {"Cardiologie"}));
			repository.save(new Hospital("Beverly Bashir", 2, new String[] {"Immunologie", "neuropathologie diagnostique"}));
			
			repository.findAll().forEach(hospital -> log.info("Preloading " + hospital));
		};
	}

}
